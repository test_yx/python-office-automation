# coding =utf-8
# @Time :2022/2/3 17:45
# @Author : zcy
# @File : word_to_pdf.py
# @SoftWare : PyCharm

import os
from win32com import client

#源地址，就是存放word文档的地址
source_path = "E:\\3pyoffice\\word2pdf\\word"
#目标地址，存放pdf的地址
target_path = "E:\\3pyoffice\\word2pdf\\pdf"
#相当于打开了一个word程序
word_app = client.Dispatch("Word.Application")


try:
    #判断源地址是否存在
    if not os.path.exists(source_path):
        print("未查询到Word文档")
    #判断目标地址是否存在
    if not os.path.exists(target_path):
        #不存在创建文件夹
        os.makedirs(target_path)
    #获取源文件夹中的word文件名称
    word_names = os.listdir(source_path)
    print("获取到" + str(len(word_names)) + "个文件")
    #遍历文件路径，读取word文档
    for word_name in word_names:
        #对文件名称根据后缀进行过滤
        filename = os.path.splitext(word_name)[0]
        suffix = os.path.splitext(word_name)[1]
        if suffix == '.docx' or suffix == '.doc':
            print("开始处理" + word_name)
            #读取对应的word文档
            word = word_app.Documents.Open(source_path + "\\" + word_name, ReadOnly=1)
            #另存为pdf
            word.SaveAs(target_path + "\\" + filename + ".pdf", FileFormat=17)
            #关闭对应的word文档
            word.Close()
            print("结束处理" + word_name)
except Exception as e:
    print(e)
