# coding =utf-8
# @Time :2022/2/11 21:11
# @Author : zcy
# @File : __init__.py.py
# @SoftWare : PyCharm

import PyPDF2


def add_watermark(target_pdf, water_mark, marked_pdf):
    pdf_input = PyPDF2.PdfFileReader(open(target_pdf, 'rb'), strict=False)
    watermark_page = PyPDF2.PdfFileReader(open(water_mark, 'rb'), strict=False).getPage(0)
    pdf_output = PyPDF2.PdfFileWriter()
    for i in range(pdf_input.getNumPages()):
        page = pdf_input.getPage(i)
        # 合并水印和源页
        page.mergePage(watermark_page)
        # 压缩
        page.compressContentStreams()
        pdf_output.addPage(page)

    pdf_output.write(open(marked_pdf, 'wb'))


if __name__ == '__main__':
    water_mark_path = r'E:\3pyoffice\pdf_water_mark\water_mark.pdf'
    target_pdf_path = r'E:\3pyoffice\pdf_water_mark\water_mark_target.pdf'
    marked_pdf_path = r'E:\3pyoffice\pdf_water_mark\water_mark_target_marked.pdf'
    add_watermark(target_pdf_path, water_mark_path, marked_pdf_path)
